﻿using T1.BD.mapping;
using T1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace T1.BD.mapping
{


    public class PostMap : IEntityTypeConfiguration<Post>

    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasKey(post => post.Id);

        }
    }
}
