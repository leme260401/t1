﻿using T1.BD.mapping;
using T1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1.BD
{
    public class AppBlogContex : DbContext
    {
        
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }


        public AppBlogContex(DbContextOptions<AppBlogContex> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PostMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());

        }
    }
}
