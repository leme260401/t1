﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1.Models
{
    public class Post
    {


        public int Id { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public DateTime fecha { get; set; }
        public string contenido { get; set; }

    }
}
